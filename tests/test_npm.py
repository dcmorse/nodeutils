import unittest, os, sys
from ..npm import NPM
import logging.config


class TestNPM(unittest.TestCase):

    def test_adhoc(self):

        # logger = logging.getLogger()
        # h1 = logging.StreamHandler(sys.stdout)
        # h1.setLevel(logging.DEBUG)
        # logger.addHandler(h1)
        # h2 = logging.StreamHandler(sys.stderr)
        # h2.setLevel(logging.WARNING)
        # logger.addHandler(h2)
        # logger.setLevel(logging.DEBUG)

        conf = logging.config.dictConfig({
            'version': 1,
            'disable_existing_loggers': False,
            'formatters': {
                'default': {
                    'format': 'NORMAL ----> %(asctime)s %(levelname)s %(name)s: %(message)s',
                },
                'panic': {
                    'format': 'PANIC! ====> %(asctime)s %(levelname)s %(name)s: %(message)s',
                }
            },
            'handlers': {
                'debugstream': {
                    # The values below are popped from this dictionary and used to create the handler, set the handler's level and its formatter.
                    '()': logging.StreamHandler,
                    'level': 'DEBUG',
                    'formatter': 'default',
                    # The values below are passed to the handler creator callable as keyword arguments.
                    'stream': sys.stdout,
                },
                'errstream': {
                    '()': logging.StreamHandler,
                    'level': 'WARNING',
                    'formatter': 'panic',
                    'stream': sys.stderr,
                },
            },
            'root': {
                'handlers': ['debugstream'],  # , 'errstream'],
                'level': 'DEBUG',
                'propagate': True,
            },
            'djangocrack.npm.NPM': ['debugstream', 'errstream'],
            'level': 'NOSET',
            'propagate': False,
        })
        logging.config.dictConfig(conf)
        node_dir = '/home/dmorse/Workspace/PycharmProjects/factsetnews-temp/factsetnews/node'
        # node_dir = os.path.join(
        #     os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))),
        #     '../node'
        # )
        npm = NPM(node_dir)
        results = npm.run('env-echo')
        _ = 0


    @unittest.skip
    def test_prefer_condaenv_node_npm(self):
        pass

    @unittest.skip
    def test_prefer_local_npm(self):
        pass

    @unittest.skip
    def test_can_install_packagejson_from_anywhere(self):
        pass

    @unittest.skip
    def test_all_npm_methods_attached(self):
        pass

    @unittest.skip("this is known to still not work")
    def test_all_npm_method_names_docstrings_correct(self):
        pass

    @unittest.skip
    def test_compatible_with_all_npm_versions(self):
        pass

    @unittest.skip
    def test_logging_async(self):
        pass

    @unittest.skip
    def test_log_output_correct(self):
        pass


if __name__ == '__main__':
    unittest.main()
