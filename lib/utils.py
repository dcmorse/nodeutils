import os, logging, re
from types import SimpleNamespace
import typing


class NPMUtils(SimpleNamespace):
    """
    A stateless namespace of static methods made for use in the main NPM class.
    """
    from collections import defaultdict
    npm_logline_levelmap = defaultdict(lambda key: logging.INFO)
    npm_logline_levelmap.update({
        # https://github.com/npm/npmlog/blob/541407008c509755255a4819606e7916d26a77f5/log.js#L296
        # https://docs.python.org/3/library/logging.html?highlight=logging%20levels#logging-levels
        "silly": logging.NOTSET,  # -Infinity
        "sill": logging.NOTSET,  # -Infinity
        "verbose": logging.DEBUG,  # 1000
        "verb": logging.DEBUG,  # 1000
        "info": logging.DEBUG,  # 2000
        "timing": logging.INFO,  # 2500
        "http": logging.INFO,  # 3000
        "notice": logging.INFO,  # 3500
        "warn": logging.WARNING,  # 4000
        "WARN": logging.WARNING,  # 4000
        "error": logging.ERROR,  # 5000
        "ERR!": logging.ERROR,  # 5000
        "silent": logging.CRITICAL,  # Infinity
    })
    npm_logline_regex = re.compile(r'^\s*(npm) (?P<level>\w+!?) (?P<logline>.+)\s*$')

    @classmethod
    def parse_npm_logline(cls, line: str) -> typing.Tuple[int, str]:
        """
        Example loglines:

        npm sill install factsetnews@1.0.0 /Users/dcmorse/Workspace/PycharmProjects/factsetnews-temp/fa...
        npm info lifecycle factsetnews@1.0.0~install: factsetnews@1.0.0
        npm sill lifecycle factsetnews@1.0.0~install: no script for install, continuing
        npm sill postinstall factsetnews@1.0.0 /Users/dcmorse/Workspace/PycharmProjects/factsetnews-t...
        npm info lifecycle factsetnews@1.0.0~postinstall: factsetnews@1.0.0

        > factsetnews@1.0.0 postinstall /Users/dcmorse/Workspace/PycharmProjects/factsetnews-temp/factsetnews/node
        > node $NODE_DEBUG_OPTION scripts/postinstall.js

        npm verb lifecycle factsetnews@1.0.0~postinstall: unsafe-perm in lifecycle true
        npm verb lifecycle factsetnews@1.0.0~postinstall: PATH: /Users/dcmorse/anaconda3/envs/factsetnews/...
        npm verb lifecycle factsetnews@1.0.0~postinstall: CWD: /Users/dcmorse/Workspace/PycharmProjects/f...
        npm sill lifecycle factsetnews@1.0.0~postinstall: Args: [ '-c', 'node

        Return a tuple (loglevel, parsedline) where loglevel is determined by the npm loglevel
        """
        loglevel, logline = 'OUTPUT', line.strip()
        m = re.fullmatch(cls.npm_logline_regex, line)
        if m:
            d = m.groupdict()
            npm_level = d['level']
            loglevel = cls.npm_logline_levelmap[npm_level]
            logline = '{}: {}'.format(npm_level, d['logline'].strip())
        return loglevel, logline


    @staticmethod
    def get_npm_path(classname) -> str:
        """This is only tested on Posix"""
        import sys, re
        npm_bin = None
        conda_bindir = re.match(r'(.+{sep}\w+conda\d?{sep}.+){sep}python'.format(sep=os.sep), sys.executable)
        if conda_bindir:
            # then we're in a conda env, and we'll prefer the NPM installed here
            _npm_bin = os.path.join(conda_bindir.groups()[0], 'npm')
            if os.path.isfile(_npm_bin):
                # then this conda env has npm installed
                npm_bin = _npm_bin
        else:
            from distutils.spawn import find_executable
            _npm_bin = find_executable('npm')
            if _npm_bin is None:
                from django.apps.config import ImproperlyConfigured
                raise ImproperlyConfigured('You need to have Node.js and npm installed in order to use {}'.format(classname))
            else:
                npm_bin = _npm_bin
        return npm_bin

    @staticmethod
    def get_package_json_absdir(package_json) -> str:
        if os.path.isdir(package_json) and os.path.isfile(os.path.join(package_json, 'package.json')):
            # they passed the correct thing
            return os.path.abspath(package_json)
        elif os.path.isfile(package_json) and os.path.basename(package_json) == 'package.json':
            # they passed the filepath and we just need the dir
            return os.path.abspath(os.path.dirname(package_json))
        else:
            raise FileNotFoundError("This doesn't appear to be a npm package directory: {}".format(package_json))


    @staticmethod
    def parse_commands_output(outputstring) -> dict:
        outputlist = [line for line in outputstring.split('\n') if line.strip()]
        regex = re.compile(r'^(?P<startspace>\s{2,})(?P<command>[\w\-]+)?(?P<midspace>\s{2,})?(?P<description_part>.+)$')
        cmddict = {}
        at_start = True
        currcommand = ''
        startspace_width = 0
        for outline in outputlist:

            m = re.match(regex, outline)
            if not m and not at_start:  # ended command list
                break
            elif not m and at_start:  # still waiting to start command list
                continue

            at_start = False
            d = m.groupdict()
            if startspace_width == 0:
                # we just started the first line and we assume there's output like:
                # "     access       npm access public [<package>]"
                startspace_width = len(d['startspace'])

            if d['midspace'] and len(d['startspace']) == startspace_width:  # then there's a new command in it
                currcommand = d['command'].replace('-', '_')
                cmddict[currcommand] = d['description_part']
            else:
                faux_command_match = d['command'] or ''
                cmddict[currcommand] += '\n' + faux_command_match + d['description_part']
        return cmddict
