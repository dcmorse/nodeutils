import asyncio as _asyncio
import os as _os
import logging as _logging
import subprocess as _subprocess
import re as _re
import functools as _functools
from types import SimpleNamespace as _SimpleNamespace
import typing as _typing
from .utils import NPMUtils as _NPMUtils


class NPMException(Exception):
    pass


class NPM(_SimpleNamespace):
    """
    A Python interface to npm
    Tested with npm@3.10.10
    """

    name = 'djangocrack.npm.NPM'
    available_commands = {}
    logger = None
    npm_bin = 'npm'

    def __new__(cls, *args, **kwargs):
        """I think doing all this in __new__ means I
        don't have to do it on new NPM instances, which
        is fine provided the npm_bin doesn't change."""

        # setup logger
        if not cls.logger:
            cls.logger = _logging.getLogger(kwargs.get('appname') or kwargs.get('app_name') or cls.name)

        # get full NPM binary path
        npm_bin = kwargs.get('npm_bin') or cls.npm_bin

        if npm_bin == cls.npm_bin == 'npm' or not _os.path.isfile(npm_bin):
            cls.npm_bin = _NPMUtils.get_npm_path(cls.name)
        elif npm_bin:
            cls.npm_bin = npm_bin
        else:
            raise NPMException("Can't find npm binary. Pass `npm_bin=<full path to npm binary>`")

        # get available commands if I haven't yet
        if not cls.available_commands:
            try:
                # this is very fragile I think so I won't worry abotu exceptions here
                cls.available_commands = cls.get_available_commands()
            except Exception as e:
                cls.logger.exception('Error getting available npm commands')
        cls._command_factory(cls.available_commands)

        return super().__new__(cls)


    def __init__(self, package):
        """
        Create a new instance of npm bound to a particular project
        :param package: the package.json file of the project, or the directory
        """
        self.package = _NPMUtils.get_package_json_absdir(package)
        # if we have a locally installed NPM we'll prefer that for this instance
        # credit to @Jay for easy exe testing: https://stackoverflow.com/a/377028/4106215
        maybe_local_npm_bin = _os.path.join(self.package, 'node_modules/.bin/npm')
        if maybe_local_npm_bin != self.npm_bin and _os.path.isfile(maybe_local_npm_bin) and _os.access(maybe_local_npm_bin, _os.X_OK):
            self.npm_bin = maybe_local_npm_bin
            self.available_commands = self.get_available_commands()
            self._command_factory(self.available_commands)


    @classmethod
    def get_available_commands(cls) -> dict:
        # ... get a list of commands
        proc = _subprocess.Popen(
            [cls.npm_bin, '--ls'],
            stdout=_subprocess.PIPE,
            stderr=_subprocess.PIPE
        )
        stdout, stderr = proc.communicate()
        if stderr:
            cls.logger.warning('Getting commands produced some error output:\n{}'.format(stderr.decode()))
        stdout_string = stdout.decode()
        cmdout_dict = _NPMUtils.parse_commands_output(stdout_string)

        find_aliases_regex = _re.compile(r'(alias(es)?: )(.+)$', flags=_re.MULTILINE)
        cmdout_alias_dict = {}
        for command, description in cmdout_dict.items():
            m = _re.findall(find_aliases_regex, description)
            if m:
                aliases = m[0][-1]
                aliaslist = map(str.strip, aliases.split(','))
                for alias in aliaslist:
                    cmdout_alias_dict.update({alias.replace('-', '_'): 'Alias of {}'.format(command)})
        cmdout_dict.update(cmdout_alias_dict)
        return cmdout_dict


    @classmethod
    def _command_factory(cls, commanddict: dict):
        for command, description in commanddict.items():
            if not hasattr(cls, command):
                setattr(cls, command, _functools.partialmethod(cls.execute, command))


    async def _do_logging(self, buffer: _asyncio.StreamReader, streamtitle: str) -> _typing.Dict[str, _typing.List]:
        non_log_output = {streamtitle: []}
        while not buffer.at_eof():
            line = await buffer.readline()
            loglevel, parsedline = _NPMUtils.parse_npm_logline(line.decode())
            if loglevel == 'OUTPUT':
                if parsedline:
                    non_log_output[streamtitle].append(parsedline)
            else:
                self.logger.log(loglevel, parsedline)
        return non_log_output


    async def _stream_subprocess(self, proc_args, **kwargs) -> dict:
        removed_kwargs = {}
        kwargskeys = kwargs.keys()
        for removekwarg in ('stdout', 'stderr', 'timeout'):
            if removekwarg in kwargskeys:
                removed_kwargs.update({removekwarg: kwargs.pop(removekwarg)})

        proc = await _asyncio.create_subprocess_exec(  # type: _asyncio.subprocess.Process
            *proc_args,
            stdout=_asyncio.subprocess.PIPE,
            stderr=_asyncio.subprocess.PIPE,
            **kwargs
        )
        done, pending = await _asyncio.wait(  # type: _typing.Tuple[_asyncio.Future, _asyncio.Future]
            [
                self._do_logging(proc.stdout, streamtitle='stdout'),
                self._do_logging(proc.stderr, streamtitle='stderr'),
            ],
            timeout=removed_kwargs.get('timeout'),
            return_when=_asyncio.ALL_COMPLETED,
        )
        returncode = await proc.wait()

        msg = 'Returncode frun asyncio subprocess was {}'.format(returncode)
        if returncode != 0:
            self.logger.warning(msg)
        else:
            self.logger.debug(msg)


        taskset = done.union(pending)  # type: _typing.Set[_asyncio.Task]
        result_dict = {}
        for task in taskset:
            taskname = []
            try:
                task_result = task.result()
                taskname = list(task_result)
                ex = task.exception()
                if ex:
                    raise ex
            except Exception as e:
                msg = 'Coroutine "{}" threw an exception during execution'
                if taskname:
                    msg = msg.format(taskname[0])
                else:
                    msg = msg.format('<UNKNOWN>')
                self.logger.exception(msg)
            result_dict.update(task.result())
        return result_dict


    dont_pass_package = ('version', 'run', 'run_script', 'run-script')

    def execute(self, command: str, *args, timeout=None, npm_kwargs=None, **popen_kwargs):
        """
        https://kevinmccarthy.org/2016/07/25/streaming-subprocess-stdin-and-stdout-with-asyncio-in-python/
        args in npm_kwargs will be passed to npm.
        for signal args just map them to a falsey value, e.g.:
        npm_args = { 'save': None, 'save-dev': None} --> --save --save-dev
        """

        # build args
        proc_args = [self.npm_bin, '--prefix={}'.format(self.package), command.replace('_', '-')] + list(args)
        kwargs_list = []
        if npm_kwargs:
            for k, v in npm_kwargs.items():
                if v:
                    kwargs_list.append('--{}={}'.format(k, v))
                else:
                    kwargs_list.append('--{}'.format(k))
        proc_args += kwargs_list
        if command not in self.dont_pass_package:
            proc_args += [self.package]
        self.logger.debug('Running NPM command: {}'.format(' '.join(proc_args)))

        loop = _asyncio.new_event_loop()
        _asyncio.set_event_loop(loop)
        result_dict = loop.run_until_complete(self._stream_subprocess(proc_args, timeout=timeout, **popen_kwargs))
        loop.close()
        return result_dict
